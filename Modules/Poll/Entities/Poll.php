<?php

namespace Modules\Poll\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelTrait;
use Modules\User\Entities\User;

class Poll extends Model
{
    use ModelTrait;

    protected $fillable = ['user_id', 'content', 'can_anonymously', 'status'];

    public function rules()
    {
        return [
            'user_id' => 'required',
            'content' => 'required',
            'can_anonymously' => 'required',
            'status' => 'required',
        ];
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
