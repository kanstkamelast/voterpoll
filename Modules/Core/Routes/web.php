<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/auth/facebook/redirect', 'CoreController@facebookAuthRedirect');
Route::get('/auth/facebook/response', 'CoreController@facebookAuthResponse');

Route::get('/{any}', [
    'as' => 'homepage',
    'uses' => 'CoreController@index'
])->where('any', '^((?!api).)*');
