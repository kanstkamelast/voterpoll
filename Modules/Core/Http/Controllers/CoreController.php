<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\User\Entities\User;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Socialite;
use Exception;
use Auth;

class CoreController extends Controller
{
    public function index()
    {
        return view('app');
    }

    public function facebookAuthRedirect(Request $request)
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function facebookAuthResponse(Request $request)
    {
        $facebookUser = Socialite::driver('facebook')->user();

        $localUser = app()->make(User::class)->findByEmailAndFacebookId($facebookUser->getEmail(), $facebookUser->getId());

        if($localUser)
        {
            $token = JWTAuth::fromUser($localUser);
        }
        else
        {
            $newUser = User::create(['facebook_id' => $facebookUser->getId(), 'name' => $facebookUser->getName(), 'email' => $facebookUser->getEmail()]);
            $token = JWTAuth::fromUser($newUser);
        }
        return redirect('/?code='.$token);
    }
}
