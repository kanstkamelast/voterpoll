<?php

namespace Modules\User\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Traits\ModelTrait;
use Modules\Poll\Entities\Poll;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, ModelTrait;

    protected $fillable = [
        'name', 'email', 'password', 'facebook_id',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function rules()
    {
        return [
            'name' => 'required',
            'email' => is_null($this->id) ? 'required|unique:users' : 'required|unique:users,email,'.$this->id,
            'facebook_id' => 'required',
        ];
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function findByEmailAndFacebookId(string $email, string $facebookId)
    {
        return $this->where('email', $email)->where('facebook_id', $facebookId)->first();
    }

    public function polls()
    {
        return $this->hasMany(Poll::class, 'user_id');
    }
}