<?php

namespace Modules\User\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\User\Entities\User;
use App\Repositories\EloquentRepositories\UserEloquentRepository as UserRepo;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

class UserController extends Controller
{
    public function __construct()
    {
    }

    public function getUserData(Request $request)
    {
        return response()->json(JWTAuth::toUser($request->token));
    }

    public function logout(Request $request)
    {
        return response()->json(JWTAuth::invalidate($request->token));
    }

}
