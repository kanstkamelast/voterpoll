export default {
    userData(state) {
        return state.userData
    },
    authorisedUser(state) {
        return state.authorisedUser
    },
    newPoll(state) {
        return state.newPoll
    },
    newPollItemTypes(state) {
        return state.newPollItemTypes
    },
    canDeleteItem(state) {
        return state.newPoll.length > 1
    },
    canAddItem(state) {
        return state.newPoll.length < 5
    },
    canAddItemVariant: state => index => state.newPoll[index].variants.length < 5,
    canDeleteItemVariant: state => index => state.newPoll[index].variants.length > 2,
}