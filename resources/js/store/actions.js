import state from "./state";

export default {
    loadUserData({commit}, token) {
        axios
            .get('/user/data?token=' + token)
            .then(response => {
                localStorage.setItem('accessToken', token);
                commit('setUserData', response.data);
                commit('setAuthorisedUser', true);
            })
            .catch(error => {
                commit('setUserData', {});
                commit('setAuthorisedUser', false);
                localStorage.removeItem('accessToken');
            });
    },
    logoutUser({commit}, token) {
        axios
            .get('/user/logout?token=' + token)
            .then(response => {
                localStorage.removeItem('accessToken');
                commit('setUserData', {});
                commit('setAuthorisedUser', false);
            })
            .catch(error => {
                console.log('error while logging out');
            });
    },

    addItemToNewPoll({commit}) {
        commit('addItemToNewPoll');
    },
    deleteItemFromNewPoll({commit}, itemId) {
        commit('deleteItemFromNewPoll', itemId);
    },
    addVariantToNewPollItem({commit}, itemId) {
        commit('addVariantToNewPollItem', itemId);
    },
    deleteVariantFromNewPollItem({commit}, indexData) {
        commit('deleteVariantFromNewPollItem', indexData);
    },
    removeCheckedItems({commit}, indexData) {
        commit('removeCheckedItems', indexData);
    },
    removeAllCheckedItems({commit}, indexData) {
        commit('removeAllCheckedItems', indexData);
    },
    addPoll({commit}) {
        axios
        .post('/poll/store', {
            //headers: { Authorization: "Bearer " + localStorage.getItem('accessToken') },
            poll: state.newPoll
        })
        .then(response => {
            console.log(response.data)
        })
        .catch(error => {
            console.log('error while logging out');
        });
    }
}