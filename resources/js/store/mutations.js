export default {
    setUserData(state, userData) {
        state.userData = userData
    },
    setAuthorisedUser(state, authorisedUser) {
        state.authorisedUser = authorisedUser
    },
    addItemToNewPoll(state) {
        state.newPoll.push({
            title: '',
            type: 'input',
            haveCorrectVariant: false,
            variants: [{}, {}],
          });
    },
    deleteItemFromNewPoll(state, itemId) {
        state.newPoll.splice(itemId, 1);
    },
    addVariantToNewPollItem(state, itemId) {
        state.newPoll[itemId].variants.push({
            title: '',
            correct: false
        });
    },
    deleteVariantFromNewPollItem(state, indexData) {
        state.newPoll[indexData.itemIndex].variants.splice(indexData.variantIndex, 1);
    },
    removeCheckedItems(state, indexData) {
        if(state.newPoll[indexData.itemIndex].type == 'radioButton')
        state.newPoll[indexData.itemIndex].variants.forEach(function(element, index) {
            if(index != indexData.variantIndex) element.correct = false
        })
    },
    removeAllCheckedItems(state, indexData) {
        state.newPoll[indexData].variants = [{}, {}]
    }

}