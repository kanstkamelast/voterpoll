export default {
    authorisedUser: false,
    userData: {},
    newPoll: [
        {
            title: '',
            type: 'input',
            haveCorrectVariant: false,
            variants: [{}, {}],
            answer: {},
        }
    ],
    newPollItemTypes: [
        {
            name: 'Short answer',
            type: 'input'
        },
        {
            name: 'Long answer',
            type: 'textarea'
        },
        {
            name: 'One choice',
            type: 'radioButton'
        },
        {
            name: 'Multiple choice',
            type: 'checkbox'
        },
    ]
}