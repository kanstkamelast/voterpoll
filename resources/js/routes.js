import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from './components/HomeComponent'
import User from './components/UserComponent'
import AddPoll from './components/user/AddPoll'

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'homepage',
            component: Home,
        },
        {
            path: '/me',
            name: 'user',
            component: User,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/add-poll',
            name: 'addPoll',
            component: AddPoll,
            meta: {
                requiresAuth: true
            }
        },
    ]    
});


export default router;