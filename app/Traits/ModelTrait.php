<?php

namespace App\Traits;

use Validator;
use App\Exceptions\ModelException;

trait ModelTrait
{
    private $errors;

    public static function bootModelTrait()
    {
        static::saving(function ($model) {

            if (!$model->validate($model->toArray(), $model->rules()))
            {
                throw new ModelException($model->errors());
            }

            !isset($model->password) ?: $model->password = bcrypt($model->password);

        });
    }

    public function validate($data, $rules)
    {
        $v = Validator::make($data, $rules);
        
        if ($v->fails())
        {
            $this->errors = $v->errors();

            return false;
        }

        return true;
    }

    public function errors()
    {
        return $this->errors;
    }
}